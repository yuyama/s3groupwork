#!/usr/bin/perl

use strict;
use warnings;

opendir(SRC, 'src/') or die "$!";
open(MAKEFILE, '>Makefile') or die "$!";

print MAKEFILE <<'EOS';
CC = g++
CFLAGS = -lcurses -g
TARGET = dd
SRCS = $(shell ls src/*.cpp)
SRC_DIR = src
OBJ_DIR = obj
OBJS = 
EOS

#OBJS
for my $filename (readdir(SRC)) {
	next unless $filename =~ /\.cpp$/;
	$filename =~ s/\.cpp/\.o/g;
	print MAKEFILE "OBJS += obj/$filename\n";
	close(FILE);
}
closedir(SRC);

print MAKEFILE <<'EOS';

all: $(OBJ_DIR) $(TARGET)

$(OBJ_DIR):
	mkdir $(OBJ_DIR)

$(TARGET): $(OBJS)
	$(CC) $(OBJS) -o $@ $(CFLAGS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CC) -c -o $@ $<

clean:
	rm dd && rm -r $(OBJ_DIR)

EOS

#dependencies
opendir(SRC, 'src/') or die "$!";
for my $filename (readdir(SRC)) {
	next unless $filename =~ /\.cpp$/;
	open(FILE, "src/$filename") or die "$!";
	for my $line (<FILE>) {
		next unless $line =~ /^#include "(.+)"$/;
		my $header = $1;
		chomp($line);
		my $objname = $filename;
		$objname =~ s/\.cpp/\.o/g;
		print MAKEFILE "obj/$objname: src/$header\n";
	}
	close(FILE);
}

close(MAKEFILE);
closedir(SRC);

