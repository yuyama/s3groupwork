CC = g++
CFLAGS = -lcurses -g
TARGET = dd
SRCS = $(shell ls src/*.cpp)
SRC_DIR = src
OBJ_DIR = obj
OBJS = 
OBJS += obj/ncwrapper.o
OBJS += obj/mymtime.o
OBJS += obj/main.o
OBJS += obj/game.o
OBJS += obj/draw.o
OBJS += obj/task.o
OBJS += obj/tasklist.o
OBJS += obj/effect.o
OBJS += obj/ship.o
OBJS += obj/bomb.o
OBJS += obj/missile.o
OBJS += obj/submarine.o
OBJS += obj/item.o
OBJS += obj/sea.o
OBJS += obj/stockitem.o
OBJS += obj/speeditem.o
OBJS += obj/taskiterator.o
OBJS += obj/input.o
OBJS += obj/scene.o
OBJS += obj/titlescene.o
OBJS += obj/informationscene.o
OBJS += obj/crasheffect.o
OBJS += obj/creditscene.o
OBJS += obj/titlesea.o
OBJS += obj/gameoverscene.o
OBJS += obj/pausescene.o
OBJS += obj/titlesubmarine.o
OBJS += obj/rankingscene.o
OBJS += obj/nameinputscene.o
OBJS += obj/fastsubmarine.o
OBJS += obj/shipcrasheffect.o
OBJS += obj/messageeffect.o
OBJS += obj/normalsubmarine.o
OBJS += obj/itemsubmarine.o
OBJS += obj/threewaysubmarine.o
OBJS += obj/tiltmissile.o
OBJS += obj/straightmissile.o

all: $(OBJ_DIR) $(TARGET)

$(OBJ_DIR):
	mkdir $(OBJ_DIR)

$(TARGET): $(OBJS)
	$(CC) $(OBJS) -o $@ $(CFLAGS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CC) -c -o $@ $<

clean:
	rm dd && rm -r $(OBJ_DIR)

obj/ncwrapper.o: src/ncwrapper.h
obj/ncwrapper.o: src/constants.h
obj/mymtime.o: src/mymtime.h
obj/main.o: src/game.h
obj/game.o: src/game.h
obj/game.o: src/ncwrapper.h
obj/game.o: src/mymtime.h
obj/game.o: src/task.h
obj/game.o: src/taskiterator.h
obj/game.o: src/scene.h
obj/game.o: src/titlescene.h
obj/game.o: src/constants.h
obj/game.o: src/sea.h
obj/game.o: src/effect.h
obj/game.o: src/gameoverscene.h
obj/game.o: src/crasheffect.h
obj/game.o: src/pausescene.h
obj/game.o: src/titlesea.h
obj/game.o: src/titlesubmarine.h
obj/game.o: src/informationscene.h
obj/game.o: src/creditscene.h
obj/game.o: src/nameinputscene.h
obj/game.o: src/item.h
obj/game.o: src/stockitem.h
obj/game.o: src/speeditem.h
obj/game.o: src/messageeffect.h
obj/game.o: src/normalsubmarine.h
obj/game.o: src/fastsubmarine.h
obj/game.o: src/itemsubmarine.h
obj/game.o: src/threewaysubmarine.h
obj/game.o: src/straightmissile.h
obj/game.o: src/tiltmissile.h
obj/draw.o: src/draw.h
obj/draw.o: src/ncwrapper.h
obj/draw.o: src/constants.h
obj/task.o: src/task.h
obj/task.o: src/tasklist.h
obj/tasklist.o: src/tasklist.h
obj/tasklist.o: src/taskiterator.h
obj/tasklist.o: src/task.h
obj/effect.o: src/effect.h
obj/effect.o: src/game.h
obj/ship.o: src/ship.h
obj/ship.o: src/game.h
obj/ship.o: src/constants.h
obj/ship.o: src/bomb.h
obj/ship.o: src/gameoverscene.h
obj/ship.o: src/shipcrasheffect.h
obj/bomb.o: src/bomb.h
obj/bomb.o: src/constants.h
obj/bomb.o: src/taskiterator.h
obj/bomb.o: src/submarine.h
obj/bomb.o: src/game.h
obj/bomb.o: src/crasheffect.h
obj/missile.o: src/missile.h
obj/missile.o: src/game.h
obj/missile.o: src/constants.h
obj/submarine.o: src/submarine.h
obj/submarine.o: src/game.h
obj/submarine.o: src/constants.h
obj/submarine.o: src/sea.h
obj/submarine.o: src/stockitem.h
obj/submarine.o: src/speeditem.h
obj/submarine.o: src/straightmissile.h
obj/item.o: src/item.h
obj/item.o: src/game.h
obj/sea.o: src/sea.h
obj/sea.o: src/constants.h
obj/sea.o: src/game.h
obj/sea.o: src/submarine.h
obj/sea.o: src/normalsubmarine.h
obj/sea.o: src/fastsubmarine.h
obj/sea.o: src/itemsubmarine.h
obj/sea.o: src/threewaysubmarine.h
obj/stockitem.o: src/stockitem.h
obj/stockitem.o: src/game.h
obj/stockitem.o: src/messageeffect.h
obj/speeditem.o: src/speeditem.h
obj/speeditem.o: src/game.h
obj/speeditem.o: src/messageeffect.h
obj/taskiterator.o: src/taskiterator.h
obj/taskiterator.o: src/tasklist.h
obj/taskiterator.o: src/task.h
obj/input.o: src/input.h
obj/input.o: src/ncwrapper.h
obj/scene.o: src/scene.h
obj/scene.o: src/game.h
obj/titlescene.o: src/titlescene.h
obj/titlescene.o: src/game.h
obj/titlescene.o: src/constants.h
obj/titlescene.o: src/titlesea.h
obj/titlescene.o: src/creditscene.h
obj/titlescene.o: src/rankingscene.h
obj/informationscene.o: src/informationscene.h
obj/informationscene.o: src/game.h
obj/informationscene.o: src/constants.h
obj/informationscene.o: src/sea.h
obj/crasheffect.o: src/crasheffect.h
obj/crasheffect.o: src/game.h
obj/creditscene.o: src/creditscene.h
obj/creditscene.o: src/game.h
obj/creditscene.o: src/titlescene.h
obj/creditscene.o: src/constants.h
obj/titlesea.o: src/titlesea.h
obj/titlesea.o: src/titlesubmarine.h
obj/titlesea.o: src/constants.h
obj/gameoverscene.o: src/gameoverscene.h
obj/gameoverscene.o: src/titlescene.h
obj/gameoverscene.o: src/game.h
obj/gameoverscene.o: src/constants.h
obj/gameoverscene.o: src/sea.h
obj/gameoverscene.o: src/informationscene.h
obj/pausescene.o: src/pausescene.h
obj/pausescene.o: src/game.h
obj/pausescene.o: src/constants.h
obj/pausescene.o: src/titlescene.h
obj/titlesubmarine.o: src/titlesubmarine.h
obj/titlesubmarine.o: src/constants.h
obj/titlesubmarine.o: src/game.h
obj/rankingscene.o: src/rankingscene.h
obj/rankingscene.o: src/game.h
obj/rankingscene.o: src/titlescene.h
obj/nameinputscene.o: src/nameinputscene.h
obj/nameinputscene.o: src/game.h
obj/nameinputscene.o: src/titlescene.h
obj/fastsubmarine.o: src/fastsubmarine.h
obj/fastsubmarine.o: src/game.h
obj/fastsubmarine.o: src/straightmissile.h
obj/shipcrasheffect.o: src/shipcrasheffect.h
obj/shipcrasheffect.o: src/game.h
obj/messageeffect.o: src/messageeffect.h
obj/messageeffect.o: src/game.h
obj/normalsubmarine.o: src/normalsubmarine.h
obj/normalsubmarine.o: src/game.h
obj/normalsubmarine.o: src/straightmissile.h
obj/itemsubmarine.o: src/itemsubmarine.h
obj/itemsubmarine.o: src/game.h
obj/itemsubmarine.o: src/stockitem.h
obj/itemsubmarine.o: src/speeditem.h
obj/threewaysubmarine.o: src/threewaysubmarine.h
obj/threewaysubmarine.o: src/constants.h
obj/threewaysubmarine.o: src/straightmissile.h
obj/threewaysubmarine.o: src/game.h
obj/threewaysubmarine.o: src/tiltmissile.h
obj/tiltmissile.o: src/tiltmissile.h
obj/tiltmissile.o: src/constants.h
obj/tiltmissile.o: src/game.h
obj/straightmissile.o: src/straightmissile.h
obj/straightmissile.o: src/game.h
