#ifndef TASK_LIST_H
#define TASK_LIST_H

class Task;
class TaskIterator;

class TaskList {
friend class Task;
friend class TaskIterator;
public:
	TaskList(int max_task_size, int max_num_task);
	~TaskList(void);
	int getNumFreeTask(void) const;
	int getNumActiveTask(void) const;
	void clearTask(void);//全タスクの消去
private:
	Task *active_task_;
	Task *free_task_;
	const int max_task_size_;
	const int max_num_task_;
	int num_free_task_;
	char *buf_;
};

#endif //TASK_LIST_H

