#include <cctype>
#include <ncurses.h>
#include "nameinputscene.h"
#include "game.h"
#include "titlescene.h"

NameInputScene::NameInputScene(void) {
	std::list<std::pair<std::string, unsigned long> > ranking = Game::gi()->getScoreRanking();
	int i = 0;
	for(std::list<std::pair<std::string, unsigned long> >::iterator it = ranking.begin(); it != ranking.end(); ++it, ++i) {
		if(it->second < Game::gi()->getScore()) {
			break;
		}
	}
	rank_ = i + 1;
}

bool NameInputScene::move(void) {
	int code = Game::gi()->input.getKeyCode();
	if(isgraph(code)) {
		if(name_.length() < MAX_NAME_LENGTH) {
			name_ += static_cast<char>(code);
		}
	}
	if(code == KEY_BACKSPACE) {
		name_ = name_.substr(0, name_.length() - 1);
	}
	if(code == '\n') {
		if(name_ != "") {
			Game::gi()->insertScoreRanking(name_, Game::gi()->getScore());
			Game::gi()->writeScoreRanking();
			Game::gi()->loadScoreRanking();
			Game::gi()->input.update();
			Game::gi()->setGenClass(Game::TITLE);
			Game::gi()->finishGame();
			return false;
		}
	}
	return true;
}

void NameInputScene::draw(void) {
	std::string gameover_text = "[ GAME OVER ]";
	Game::gi()->draw.printString((SCREEN_WIDTH - gameover_text.length()) / 2, 6, gameover_text.c_str());
	std::string text = "INPUT YOUR NAME(PRESS ENTER KEY TO QUIT)";
	Game::gi()->draw.printString((SCREEN_WIDTH - text.length()) / 2, 8, text.c_str());
	Game::gi()->draw.printString((SCREEN_WIDTH - 20) / 2, 10, "%2d %8s %08d", rank_, name_.c_str(), Game::gi()->getScore());
}

