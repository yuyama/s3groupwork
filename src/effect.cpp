#include "effect.h"
#include "game.h"

Effect::Effect(void) : Task(Game::gi()->effect_list) {
}

void* Effect::operator new(std::size_t t) throw() {
	return operator_new(t, Game::gi()->effect_list);
}

void Effect::operator delete(void *p) {
	operator_delete(p, Game::gi()->effect_list);
}

