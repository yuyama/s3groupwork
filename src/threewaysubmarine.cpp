#include <cstdlib>
#include "threewaysubmarine.h"
#include "constants.h"
#include "straightmissile.h"
#include "game.h"
#include "tiltmissile.h"

ThreeWaySubmarine::ThreeWaySubmarine(int y, int direction, int speed, int interval) : Submarine(y, direction, speed, interval) {
}

ThreeWaySubmarine::~ThreeWaySubmarine(void) {
}

bool ThreeWaySubmarine::move(void) {
	//移動する処理
	if(!Submarine::move()) return false;
	//ミサイル発射処理
	processMissile();
	return true;
}

void ThreeWaySubmarine::launchMissile(void) {
	if (direction_ == RIGHT_AHEAD) {
		new StraightMissile(x_ +1, y_);
		new TiltMissile(x_ +1, y_, LEFT_AHEAD);
		new TiltMissile(x_ +1, y_, RIGHT_AHEAD);
	} else if (direction_ == LEFT_AHEAD) {
		new StraightMissile(x_ +5, y_);
		new TiltMissile(x_ +5, y_, LEFT_AHEAD);
		new TiltMissile(x_ +5, y_, RIGHT_AHEAD);
	}
}

void ThreeWaySubmarine::draw(void) {
	if (direction_ == RIGHT_AHEAD) {
		Game::gi()->draw.printString(x_, y_, "m9(^o^)");
	} else if (direction_ == LEFT_AHEAD) {
		Game::gi()->draw.printString(x_, y_, "(^o^)9m");
	}
}

void ThreeWaySubmarine::destroy(void) {
	Game::gi()->addScore(THREE_WAY_SCORE);  //スコア追加処理
}
