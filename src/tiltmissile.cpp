#include "tiltmissile.h"
#include "constants.h"
#include "game.h"

TiltMissile::TiltMissile(int x, int y, int direction) : Missile(x, y), direction_(direction) {
}

bool TiltMissile::move(void) {
	if(counter_ == 0) {
		y_--;
		if(direction_ == LEFT_AHEAD) {
			--x_;
		}
		if(direction_ == RIGHT_AHEAD) {
			++x_;
		}
		counter_++;
	} else {
		counter_ = 0;
	}
	if(!Missile::move()) return false;
	return true;
}

void TiltMissile::draw(void) {
	if(direction_ == LEFT_AHEAD) {
		Game::gi()->draw.printString(x_, y_, "\\");
	}
	if(direction_ == RIGHT_AHEAD) {
		Game::gi()->draw.printString(x_, y_, "/");
	}
}

