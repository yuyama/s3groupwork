#ifndef THREE_WAY_SUBMARINE_H
#define THREE_WAY_SUBMARINE_H

#include "submarine.h"

class ThreeWaySubmarine : public Submarine {
public:
	ThreeWaySubmarine(int y, int direction, int speed, int interval);
	~ThreeWaySubmarine(void);
	bool move(void);
	void draw(void);
	void destroy(void);
private:
	void launchMissile(void);
};

#endif //THREE_WAY_SUBMARINE_H

