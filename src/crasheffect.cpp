#include <string>
#include "crasheffect.h"
#include "game.h"

CrashEffect::CrashEffect(int x, int y) : x_(x), y_(y), counter_(0) {
}

bool CrashEffect::move(void) {
	++counter_;
	if(5 <= counter_) return false;
	return true;
}

void CrashEffect::draw(void) {
	std::string str;
	switch(counter_) {
		case 0: str = "  <+>  "; break;
		case 1: str = " <<+>> "; break;
		case 2: str = "<<-+->>"; break;
		case 3: str = "<--+-->"; break;
		case 4: str = "---+---"; break;
		default:
			return;
	}
	Game::gi()->draw.printString(x_ - 3, y_, str.c_str());
}

