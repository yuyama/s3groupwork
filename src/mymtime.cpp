#include "mymtime.h"

void mysleep(int msec) {
	int err;
	struct timespec req,rem;

	if ( msec < 0 || msec > 1000 ) {
		app_exit();
		fprintf( stderr, "%d is not in scale mysleep function\n", msec);
		perror("mysleep");
		_Exit(EXIT_FAILURE);
	}
	req.tv_sec = 0;
	req.tv_nsec = (long int) msec * 1000000;

	do{
		err = nanosleep( &req, &rem );
		if ( err == -1 ) {
			if ( errno != EINTR ) {
				app_exit();
				perror("nanosleep in mysleep");
				_Exit(EXIT_FAILURE);
			} else {
				req = rem;
			}
		}
	}while(err != 0);
	return;
}
