#ifndef INFORMATION_H
#define INFORMATION_H

#include "scene.h"
#include "game.h"

class InformationScene : public Scene {
public:
	bool move(void);
	void draw(void);
};

#endif //INFORMATION_H

