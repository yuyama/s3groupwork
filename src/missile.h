#ifndef MISSILE_H
#define MISSILE_H

#include "task.h"

class Missile:public Task{
 public:
  Missile(int x, int y);
  void* operator new(std::size_t t) throw();
  void operator delete(void *p);
  virtual bool move(void);
  virtual void draw(void) = 0;

 protected:
 int x_;
 int y_;
 int counter_;

};

#endif //MISSILE_H

