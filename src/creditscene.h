#ifndef CREDIT_SCENE_H
#define CREDIT_SCENE_H

#include "scene.h"

class CreditScene : public Scene {
public:
	bool move(void);
	void draw(void);
};

#endif //CREDIT_SCENE_H

