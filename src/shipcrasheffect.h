#ifndef SHIP_CRASH_EFFECT_H
#define SHIP_CRASH_EFFECT_H

#include "crasheffect.h"

class ShipCrashEffect : public CrashEffect {
public:
	ShipCrashEffect(int x, int y);
	~ShipCrashEffect(void);
};

#endif //SHIP_CRASH_EFFECT_H

