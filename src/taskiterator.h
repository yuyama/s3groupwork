#ifndef TASK_ITERATOR_H
#define TASK_ITERATOR_H

class TaskList;
class Task;

class TaskIterator {
public:
	TaskIterator(TaskList *task_list);
	bool hasNext(void) const;
	Task* next(void);
	void remove(void);
protected:
	TaskList *task_list_;
	Task *task_;
};

#endif //TASK_ITERATOR_H

