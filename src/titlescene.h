#ifndef TITLE_SCENE_H
#define TITLE_SCENE_H

#include "scene.h"

class TitleScene : public Scene {
public:
	TitleScene(void);
	TitleScene(int cursor_pos);
	bool move(void);
	void draw(void);
private:
	int cursor_pos_;//カーソル位置(何番目のメニューを指しているか、0から数える)
};

#endif //TITLE_SCENE_H

