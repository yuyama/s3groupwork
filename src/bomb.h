#ifndef BOMB_H
#define BOMB_H

#include "task.h"
#include "game.h"

class Bomb : public Task {
	public:
		Bomb(int x, int speed);
		void* operator new(std::size_t t) throw();
		void operator delete(void *p);
		bool move(void);
		void draw(void);
	private:
		int x_;
		int y_;
		int speed_;//速度の逆数[lines/frame]
		int counter_;//速度制御用のカウンタ
		bool checkHitSubmarine(void);
};

#endif //BOMB_H

