#ifndef PAUSE_SCENE_H
#define PAUSE_SCENE_H

#include "scene.h"

class PauseScene : public Scene {
public:
	bool move(void);
	void draw(void);
};

#endif //PAUSE_SCENE_H

