#ifndef FAST_SUBMARINE_H
#define FAST_SUBMARINE_H

#include "submarine.h"

class FastSubmarine : public Submarine {
public:
	FastSubmarine(int y, int direction, int speed, int interval);
	~FastSubmarine(void);
	bool move(void);
	void draw(void);
	void destroy(void);
};

#endif //FAST_SUBMARINE_H

