#ifndef NORMAL_SUBMARINE_H
#define NORMAL_SUBMARINE_H

#include "submarine.h"

class NormalSubmarine : public Submarine {
public:
	NormalSubmarine(int y, int direction, int speed, int interval);
	~NormalSubmarine(void);
	bool move(void);
	void draw(void);
	void destroy(void);
};

#endif //NORMAL_SUBMARINE_H

