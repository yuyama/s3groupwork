#include "titlesubmarine.h"
#include "constants.h"
#include "game.h"

TitleSubmarine::TitleSubmarine(int y, int direction, int speed) : Submarine(y, direction, speed, 0) {
}

void TitleSubmarine::draw(void) {
	if (direction_ == RIGHT_AHEAD) {
		Game::gi()->draw.printString(x_, y_, "=>=====");
	} else if (direction_ == LEFT_AHEAD) {
		Game::gi()->draw.printString(x_, y_, "=====<=");
	}
}

