#include "straightmissile.h"
#include "game.h"

StraightMissile::StraightMissile(int x, int y) : Missile(x, y) {
}

bool StraightMissile::move(void) {
	if(counter_ == 0) {
		y_--;
		counter_++;
	} else {
		counter_ = 0;
	}
	if(!Missile::move()) return false;
	return true;
}

void StraightMissile::draw(void) {
	Game::gi()->draw.printString(x_, y_, "|");
}

