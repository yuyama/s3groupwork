#include <cstdlib>
#include "normalsubmarine.h"
#include "game.h"
#include "straightmissile.h"

NormalSubmarine::NormalSubmarine(int y, int direction, int speed, int interval) : Submarine(y, direction, speed, interval) {
}

NormalSubmarine::~NormalSubmarine(void) {
}

bool NormalSubmarine::move(void) {
	//移動する処理
	if(!Submarine::move()) return false;
	//ミサイル発射処理
	processMissile();
	return true;
}

void NormalSubmarine::draw(void) {
	if (direction_ == RIGHT_AHEAD) {
		Game::gi()->draw.printString(x_, y_, "=>=====");
	} else if (direction_ == LEFT_AHEAD) {
		Game::gi()->draw.printString(x_, y_, "=====<=");
	}
}

void NormalSubmarine::destroy(void) {
	Game::gi()->addScore(NORMAL_SCORE);  //スコア追加処理
}

