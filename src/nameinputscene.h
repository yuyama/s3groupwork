#ifndef NAME_INPUT_SCENE_H
#define NAME_INPUT_SCENE_H

#include "scene.h"

class NameInputScene : public Scene {
public:
	NameInputScene(void);
	bool move(void);
	void draw(void);
private:
	std::string name_;
	int rank_;
};

#endif //NAME_INPUT_SCENE_H

