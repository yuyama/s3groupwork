#include "messageeffect.h"
#include "game.h"

MessageEffect::MessageEffect(std::string message) : counter_(0), message_(message) {
}

bool MessageEffect::move(void) {
	++counter_;
	if(20 < counter_) {
		return false;
	}
	return true;
}

void MessageEffect::draw(void) {
	Game::gi()->draw.printString((SCREEN_WIDTH - message_.length()) / 2, SCREEN_HEIGHT - WATER_LEVEL / 2, message_.c_str());
}

