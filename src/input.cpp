#include "input.h"
#include "ncwrapper.h"

Input::Input(void) : left_(false), right_(false), bomb_left_(false), bomb_right_(false), pause_(false), quit_(false), key_code_(0) {
}

void Input::update(void) {
	left_ = false;
	right_ = false;
	bomb_left_ = false;
	bomb_right_ = false;
	pause_ = false;
	quit_ = false;
	key_code_ = 0;
	switch(key_code_ = getch()) {
		case '4':
		case 'a':
		case KEY_LEFT:
			left_ = true;
			break;
		case '6':
		case 'd':
		case KEY_RIGHT:
			right_ = true;
			break;
		case 'z':
		case 'f':
		case ' ':
			bomb_left_ = true;
			break;
		case 'x':
		case 'j':
		case '\n':
			bomb_right_ = true;
			break;
		case 'p':
			pause_ = true;
			break;
		case 'q':
			quit_ = true;
			break;
	}
}

bool Input::getLeft(void) const {
	return left_;
}

bool Input::getRight(void) const {
	return right_;
}

bool Input::getBombLeft(void) const {
	return bomb_left_;
}

bool Input::getBombRight(void) const {
	return bomb_right_;
}

bool Input::getPause(void) const {
	return pause_;
}

bool Input::getQuit(void) const {
	return quit_;
}

int Input::getKeyCode(void) const {
	return key_code_;
}

