#ifndef RANKING_SCENE_H
#define RANKING_SCENE_H

#include "scene.h"

class RankingScene : public Scene {
public:
	bool move(void);
	void draw(void);
};

#endif //RANKING_SCENE_H

