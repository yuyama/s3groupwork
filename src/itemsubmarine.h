#ifndef ITEM_SUBMARINE_H
#define ITEM_SUBMARINE_H

#include "submarine.h"

class ItemSubmarine : public Submarine {
public:
	ItemSubmarine(int y, int direction, int speed);
	~ItemSubmarine(void);
	bool move(void);
	void draw(void);
	void destroy(void);
};

#endif //ITEM_SUBMARINE_H

