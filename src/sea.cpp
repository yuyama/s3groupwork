#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include "sea.h"
#include "constants.h"
#include "game.h"
#include "submarine.h"
#include "normalsubmarine.h"
#include "fastsubmarine.h"
#include "itemsubmarine.h"
#include "threewaysubmarine.h"

Sea::Sea(void) : Task(Game::gi()->sea_list) {
	Game::gi()->setSea(this);
	for(int i = 0; i < WATER_LEVEL - 1; ++i) {
		free_levels_.push_back(true);
	}
}

Sea::~Sea(void) {
	Game::gi()->setSea(NULL);
}

bool Sea::move(void) {
	//特定の時間ごとにSubmarineをnewする処理を書く(ゲームバランスは後で考える)
	//一応の処理
	int l = getAvailableLine();
	int speed;

	if (l <= SAFETY_SEA_LEVEL) {
	  return true;
	}

	speed  = rand() % MAX_SPEED;
	if(speed == 0){
	  speed = BASIC_SPEED;
	}

	if(l != -1 && rand() % 16 == 0) {
		const int interval = std::max(70 - (Game::gi()->getScore() / (THREE_WAY_TURN_UP / 10)), 10);
	  if((free_levels_.size() - 3) - (Game::gi()->getScore() - THREE_WAY_TURN_UP) / (THREE_WAY_TURN_UP / 5) <= l && l <= free_levels_.size() - 1) {
		  if(Game::gi()->getScore() >= THREE_WAY_TURN_UP) {
			  disableLine(l);
			  new ThreeWaySubmarine(SCREEN_HEIGHT - WATER_LEVEL + 1 + l, rand() % 2, MAX_SPEED, interval);
		  }
	  } else {
		  switch(rand() % 8) {
			  case 0:
			  case 1:
			  case 2:
			  case 3:
			  case 4:
			  case 5:
				  disableLine(l);
				  new NormalSubmarine(SCREEN_HEIGHT - WATER_LEVEL + 1 + l, rand() % 2, speed, interval);
				  break;
			  case 6:
				  if(Game::gi()->getScore() >= FAST_TURN_UP){
					  disableLine(l);
					  new FastSubmarine(SCREEN_HEIGHT - WATER_LEVEL + 1 + l, rand() % 2, 1, interval);
				  }
				  break;
			  case 7:
				  disableLine(l);
				  new ItemSubmarine(SCREEN_HEIGHT - WATER_LEVEL + 1 + l, rand() % 2, speed);
				  break;
		  }
	  }
	}
	return true;
	
}

void Sea::draw(void) {
	for(int i = 0; i < SCREEN_WIDTH; ++i) {
		Game::gi()->draw.printString(i, SCREEN_HEIGHT - WATER_LEVEL, "~");
	}
}

int Sea::getAvailableLine(void) {
	//rからたどっていって一番近くにある空行を返す
	int r = rand() % free_levels_.size();
	bool f = false;
	for(int i = r; i != r || f == false; i = (i + 1) % free_levels_.size(), f = true) {
		if(free_levels_.at(i)) {
			return r;
		}
	}
	return -1;
}

void Sea::enableLine(int line) {
	free_levels_.at(line) = true;
}

void Sea::disableLine(int line) {
	free_levels_.at(line) = false;
}

void* Sea::operator new(std::size_t t) throw() {
	return operator_new(t, Game::gi()->sea_list);
}

void Sea::operator delete(void *p) {
	operator_delete(p, Game::gi()->sea_list);
}

