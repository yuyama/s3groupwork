#ifndef INPUT_H
#define INPUT_H

class Input {
public:
	Input(void);
	void update(void);
	bool getLeft(void) const;
	bool getRight(void) const;
	bool getBombLeft(void) const;
	bool getBombRight(void) const;
	bool getPause(void) const;
	bool getQuit(void) const;
	int getKeyCode(void) const;
private:
	bool left_, right_, bomb_left_, bomb_right_, pause_, quit_;
	int key_code_;
};

#endif //INPUT_H

