#include "ship.h"
#include "game.h"
#include "constants.h"
#include "bomb.h"
#include "gameoverscene.h"
#include "shipcrasheffect.h"

Ship::Ship(void) : Task(Game::gi()->ship_list), x_(SCREEN_WIDTH / 2), stock_(DEFAULT_MAX_STOCK), max_stock_(DEFAULT_MAX_STOCK), bomb_speed_(DEFAULT_BOMB_SPEED) {
	Game::gi()->setShip(this);
}

Ship::~Ship(void) {
	Game::gi()->setShip(NULL);
	new ShipCrashEffect(x_, SCREEN_HEIGHT - WATER_LEVEL);
}

bool Ship::move(void) {
	//操作の処理
	//移動
	if(Game::gi()->input.getLeft()) {
		--x_;
	}
	if(Game::gi()->input.getRight()) {
		++x_;
	}
	if(x_ < 0) {
		x_ = 0;
	}
	if(SCREEN_WIDTH - SHIP_LENGTH < x_) {
		x_ = SCREEN_WIDTH - SHIP_LENGTH;
	}
	//爆弾投下
	if(Game::gi()->input.getBombLeft()) {
		if(0 < stock_) {
			new Bomb(x_, bomb_speed_);
			--stock_;
		}
	}
	if(Game::gi()->input.getBombRight()) {
		if(0 < stock_) {
			new Bomb(x_ + SHIP_LENGTH - 1, bomb_speed_);
			--stock_;
		}
	}
	return true;
}

void Ship::draw(void) {
	//  _
	//L/ \_
	//\___/
	Game::gi()->draw.printString(x_, SCREEN_HEIGHT - WATER_LEVEL - 2, "  _  ");
	Game::gi()->draw.printString(x_, SCREEN_HEIGHT - WATER_LEVEL - 1, "L/ \\_");
	Game::gi()->draw.printString(x_, SCREEN_HEIGHT - WATER_LEVEL - 0, "\\___/");
}

int Ship::getX(void) const {
	return x_;
}

int Ship::getStock(void) const {
	return stock_;
}

int Ship::getMaxStock(void) const {
	return max_stock_;
}

void Ship::reloadStock(void) {
	if(stock_ < max_stock_) {
		++stock_;
	}
}

void Ship::incrementMaxStock(void) {
	if(max_stock_ < TRULY_MAX_STOCK) {
		++max_stock_;
		++stock_;
	}
}

void Ship::increaseSpeed(void) {
	if(1 < bomb_speed_) --bomb_speed_;
}

void* Ship::operator new(std::size_t t) throw() {
	return operator_new(t, Game::gi()->ship_list);
}

void Ship::operator delete(void *p) {
	operator_delete(p, Game::gi()->ship_list);
}

