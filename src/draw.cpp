#include <cstdarg>
#include "draw.h"
#include "ncwrapper.h"
#include "constants.h"

Draw::Draw(void) {
}

//参照
//http://www.mkssoftware.com/docs/man3/curs_printw.3.asp
void Draw::printString(int x, int y, const char *fmt, ...) {
	va_list args;
	char result[SCREEN_WIDTH + 1];
	if(y < 0 || SCREEN_HEIGHT <= y) return;
	va_start(args, fmt);
	vsnprintf(result, SCREEN_WIDTH + 1, fmt, args);
	std::string str = result;
	va_end(args);
	if(x < 0 && -x < str.length()) {//左端が切れているとき
		mvprintw(ry_ + y, rx_ + 0, str.substr(-x).c_str());
	} else if(SCREEN_WIDTH <= x + static_cast<signed int>(str.length()) && x < SCREEN_WIDTH) {//右端が切れているとき
		mvprintw(ry_ + y, rx_ + x, str.substr(0, SCREEN_WIDTH - x).c_str());
	} else {//その他
		if(x < 0) {//左端行き過ぎているとき
			return;
		}
		if(SCREEN_WIDTH <= x) {//右端行き過ぎているとき
			return;
		}
		mvprintw(ry_ + y, rx_ + x, str.c_str());
	}
}

void Draw::clearScreen(void) {
	clear();
	get_screen_range(wy_, wx_);
	rx_ = (wx_ - SCREEN_WIDTH) / 2;
	ry_ = (wy_ - SCREEN_HEIGHT) / 2;
	for(int i = 0; i < SCREEN_WIDTH; ++i) {
		mvprintw(ry_ + -1, rx_ + i, "-");
		mvprintw(ry_ + SCREEN_HEIGHT, rx_ + i, "-");
	}
	for(int i = 0; i < SCREEN_HEIGHT; ++i) {
		mvprintw(ry_ + i, rx_ + -1, "|");
		mvprintw(ry_ + i, rx_ + SCREEN_WIDTH, "|");
	}
	mvprintw(ry_ + -1, rx_ + -1, "+");
	mvprintw(ry_ + -1, rx_ + SCREEN_WIDTH, "+");
	mvprintw(ry_ + SCREEN_HEIGHT, rx_ + -1, "+");
	mvprintw(ry_ + SCREEN_HEIGHT, rx_ + SCREEN_WIDTH, "+");
}


