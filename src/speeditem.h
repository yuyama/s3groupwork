#ifndef SPEED_ITEM_H
#define SPEED_ITEM_H

#include "item.h"

class SpeedItem : public Item {
public:
	SpeedItem(int x, int y);
	bool move(void);
	void draw(void);
};

#endif //SPEED_ITEM_H

