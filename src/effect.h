#ifndef EFFECT_H
#define EFFECT_H

#include "task.h"

class Effect : public Task {
public:
	Effect(void);
	void* operator new(std::size_t t) throw();
	void operator delete(void *p);
};

#endif //EFFECT_H

