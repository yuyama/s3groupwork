#include <cstdlib>
#include "titlesea.h"
#include "titlesubmarine.h"
#include "constants.h"

bool TitleSea::move(void) {
	if(rand() % 16 == 0) {
		int l = getAvailableLine();
		if (l <= SAFETY_SEA_LEVEL) {
		  return true;
		}
		if(l != -1) {
			new TitleSubmarine(SCREEN_HEIGHT - WATER_LEVEL + 1 + l, rand() % 2, 3 + rand() % 4);
			disableLine(l);
		}
	}
	return true;
}

