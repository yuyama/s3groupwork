#ifndef GAMEOVER_SCENE_H
#define GAMEOVER_SCENE_H

#include "scene.h"

class GameoverScene : public Scene {
public:
	GameoverScene(void);
	bool move(void);
	void draw(void);
};

#endif //GAMEOVER_SCENE_H

