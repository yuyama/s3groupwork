#include <string>
#include "pausescene.h"
#include "game.h"
#include "constants.h"
#include "titlescene.h"

bool PauseScene::move(void) {
	if(Game::gi()->input.getPause()) {
		Game::gi()->togglePause();
	}
	if(Game::gi()->getPaused()) {
		if(Game::gi()->input.getQuit()) {
			Game::gi()->togglePause();
			Game::gi()->setGenClass(Game::TITLE);
			Game::gi()->finishGame();
			Game::gi()->input.update();
			return false;
		}
	}
	return true;
}

void PauseScene::draw(void) {
	if(Game::gi()->getPaused()) {
		std::string text = "PAUSE";
		Game::gi()->draw.printString((SCREEN_WIDTH - text.length()) / 2, SCREEN_HEIGHT / 2 - 1, text.c_str());
		text = "PUSH P KEY TO RESUME";
		Game::gi()->draw.printString((SCREEN_WIDTH - text.length()) / 2, SCREEN_HEIGHT / 2, text.c_str());
		text = "PUSH Q KEY TO QUIT GAME";
		Game::gi()->draw.printString((SCREEN_WIDTH - text.length()) / 2, SCREEN_HEIGHT / 2 + 1, text.c_str());
	}
}

