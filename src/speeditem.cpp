#include "speeditem.h"
#include "game.h"
#include "messageeffect.h"

SpeedItem::SpeedItem(int x, int y) : Item(x, y) {
}

bool SpeedItem::move(void) {
	//浮上動作
	if(SCREEN_HEIGHT - WATER_LEVEL < y_) {
		if(count_ % 4 == 0) {
			--y_;
		}
	}
	//自機との当たり判定
	if(y_ == SCREEN_HEIGHT - WATER_LEVEL) {
		if(Game::gi()->getShip() == NULL) return true;
		int x = Game::gi()->getShip()->getX();
		if(x <= x_ && x_ < x + SHIP_LENGTH) {
			Game::gi()->getShip()->increaseSpeed();
			new MessageEffect("[ BOMB SPEED UP! ]");
			return false;
		}
	}
	//そのうち消える
	if(160 < count_) return false;
	++count_;
	return true;
}

void SpeedItem::draw(void) {
	if(count_ < 120 || count_ % 2 == 0) {
		Game::gi()->draw.printString(x_, y_, "&");
	}
}
