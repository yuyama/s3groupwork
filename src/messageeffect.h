#ifndef MESSAGE_EFFECT_H
#define MESSAGE_EFFECT_H

#include <string>
#include "effect.h"

class MessageEffect : public Effect {
public:
	MessageEffect(std::string message);
	bool move(void);
	void draw(void);
private:
	int counter_;
	std::string message_;
};

#endif //MESSAGE_EFFECT_H

