#ifndef TITLE_SUBMARINE_H
#define TITLE_SUBMARINE_H

#include "submarine.h"

class TitleSubmarine : public Submarine {
public:
	TitleSubmarine(int y, int direction, int speed);
	void draw(void);
};

#endif //TITLE_SUBMARINE_H

