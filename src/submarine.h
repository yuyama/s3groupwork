#ifndef SUBMARINE_H
#define SUBMARINE_H

#include "task.h"

class Submarine : public Task {
public:
	Submarine(int y, int direction, int speed, int interval);
	virtual ~Submarine(void);
	void* operator new(std::size_t t) throw();
	void operator delete(void *p);
	virtual bool move(void);
	virtual void draw(void) = 0;
	int getX(void);
	int getY(void);
	int getSpeed(void);
	virtual void destroy(void);//破壊時の処理
protected:
	void processMissile(void);//ミサイル発射処理
	virtual void launchMissile(void);//ミサイルをnew
	int x_;
	const int y_;//enableLineする関係上
	int direction_; // RIGHT_AHEAD or LEFT_AHEAD
	int speed_; // move 1/speed_ frame
	int speed_temp_;
	int counter_;//ミサイルを打たないinterval設定用のカウンタ
	const int interval_;//次にミサイルを打てるようになるまでの間隔
};

#endif //SUBMARINE_H

