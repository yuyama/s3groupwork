#include <cstdio>
#include <new>
#include "task.h"
#include "tasklist.h"

Task::Task(TaskList *task_list) : task_list_(task_list) {
	prev_ = task_list->active_task_->prev_;
	next_ = task_list->active_task_;
	prev_->next_ = this;
	next_->prev_ = this;
}

Task::~Task(void) {
	prev_->next_ = next_;
	next_->prev_ = prev_;
}

bool Task::move(void) {
	return true;
}

void Task::draw(void) {
}

void* Task::operator_new(std::size_t t, TaskList *task_list) {
	if(static_cast<std::size_t>(task_list->max_task_size_) < t) {
		fprintf(stderr, "error: too large task\n");
	}
	if(task_list->num_free_task_ <= 0) {
		return 0;//NULL
	}
	Task *task = task_list->free_task_->next_;
	task_list->free_task_->next_ = task->next_;
	--task_list->num_free_task_;
	//オーバーロードされたoperator newが返したアドレスを
	//thisポインタとしてコンストラクタが自動的に呼ばれる
	return task;//コンストラクタへ
}

void Task::operator_delete(void *p, TaskList *task_list) {
	Task *task = static_cast<Task*>(p);
	task->next_ = task_list->free_task_->next_;
	task_list->free_task_->next_ = task;
	++task_list->num_free_task_;
	if(task_list->max_num_task_ < task_list->num_free_task_) {
		fprintf(stderr, "error: task list underflowed\n");
	}
}

