#include <string>
#include "creditscene.h"
#include "game.h"
#include "titlescene.h"
#include "constants.h"

bool CreditScene::move(void) {
	//何か押されたらタイトルに戻る
	if(Game::gi()->input.getLeft() ||
			Game::gi()->input.getRight() ||
			Game::gi()->input.getBombLeft() ||
			Game::gi()->input.getBombRight() ||
			Game::gi()->input.getPause() ||
			Game::gi()->input.getQuit()) {
		Game::gi()->input.update();
		new TitleScene(2);
		return false;
	}
	return true;
}

void CreditScene::draw(void) {
	std::string names[4] = {"18 Sakakibara Kazushi", "30 Takenaka Yuto", "34 Hikima Tatsuya", "43 Yuyama Takao"};
	std::string jobs[4] = {"Programmer & Debugger", "Programmer & Debugger", "Programmer & Designer", "Chief Programmer"};
	for(int i = 0; i < 4; ++i) {
		Game::gi()->draw.printString((SCREEN_WIDTH - names[i].length()) / 2, 8 + i * 3, names[i].c_str());
		Game::gi()->draw.printString((SCREEN_WIDTH - jobs[i].length()) / 2, 8 + i * 3 + 1, jobs[i].c_str());
	}
}

