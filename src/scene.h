#ifndef SCENE_H
#define SCENE_H

#include <iostream>
#include "task.h"

class Scene : public Task {
public:
	Scene(void);
	void* operator new(std::size_t t) throw();
	void operator delete(void *p);
};

#endif //SCENE_H

