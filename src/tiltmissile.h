#ifndef TILT_MISSILE_H
#define TILT_MISSILE_H

#include "missile.h"

class TiltMissile : public Missile {
public:
	TiltMissile(int x, int y, int direction);
	bool move(void);
	void draw(void);
private:
	int direction_;
};

#endif //TILT_MISSILE_H

