#include <cstdlib>
#include <ctime>
#include <fstream>
#include "game.h"
#include "ncwrapper.h"
#include "mymtime.h"
#include "task.h"
#include "taskiterator.h"
#include "scene.h"
#include "titlescene.h"
#include "constants.h"
#include "sea.h"
#include "effect.h"
#include "gameoverscene.h"
#include "crasheffect.h"
#include "pausescene.h"
#include "titlesea.h"
#include "titlesubmarine.h"
#include "informationscene.h"
#include "creditscene.h"
#include "nameinputscene.h"
#include "item.h"
#include "stockitem.h"
#include "speeditem.h"
#include "messageeffect.h"
#include "normalsubmarine.h"
#include "fastsubmarine.h"
#include "itemsubmarine.h"
#include "threewaysubmarine.h"
#include "straightmissile.h"
#include "tiltmissile.h"

Game* Game::gi(void) {
	static Game instance;
	return &instance;
}

void Game::run(void) {
	//初期化処理
	app_init();
	//乱数の初期化
	srand(time(0));
	//タスクリストの初期化
	scene_list = new TaskList(Game::max(6, sizeof(Scene), sizeof(TitleScene), sizeof(GameoverScene), sizeof(PauseScene), sizeof(CreditScene), sizeof(NameInputScene)), MAX_NUM_SCENE);
	sea_list = new TaskList(max(2, sizeof(Sea), sizeof(TitleSea)), MAX_NUM_SEA);
	sea_ = NULL;
	ship_list = new TaskList(sizeof(Ship), MAX_NUM_SHIP);
	ship_ = NULL;
	bomb_list = new TaskList(sizeof(Bomb), MAX_NUM_BOMB);
	submarine_list = new TaskList(max(6, sizeof(Submarine), sizeof(NormalSubmarine), sizeof(FastSubmarine), sizeof(ItemSubmarine), sizeof(ThreeWaySubmarine), sizeof(TitleSubmarine)), MAX_NUM_SUBMARINE);
	missile_list = new TaskList(max(3, sizeof(Missile), sizeof(StraightMissile), sizeof(TiltMissile)), MAX_NUM_MISSILE);
	effect_list = new TaskList(max(3, sizeof(Effect), sizeof(CrashEffect), sizeof(MessageEffect)), MAX_NUM_EFFECT);
	item_list = new TaskList(max(3, sizeof(Item), sizeof(StockItem), sizeof(SpeedItem)), MAX_NUM_ITEM);
	//フラグの初期化
	finished_ = false;
	gen_class_ = NONE;
	paused_ = false;
	finished_program_ = false;
	slow_ = false;
	slow_ = 0;
	//スコア関係
	loadScoreRanking();
	resetScore();
	//必要なものをnew
	new TitleScene();
	//ncurses関係
	nodelay(stdscr, TRUE);
	keypad(stdscr, true);
	curs_set(0);//環境によっては動作しない可能性あり
	//メインループ
	while(true) {
		input.update();
		//仮のmove処理
		moveAllTaskList();
		if(finished_program_) break;
		//draw系
		draw.clearScreen();
		drawAllTaskList();
		refresh();
		mysleep(SLEEP_TIME);
	}
	//終了処理
	scene_list->clearTask();
	delete scene_list;
	ship_list->clearTask();
	delete ship_list;
	bomb_list->clearTask();
	delete bomb_list;
	submarine_list->clearTask();
	delete submarine_list;
	missile_list->clearTask();
	delete missile_list;
	sea_list->clearTask();//~Submarine()がenableLine()を呼ぶのでSubmarineより後
	delete sea_list;
	effect_list->clearTask();
	delete effect_list;
	item_list->clearTask();
	delete item_list;
	app_exit();
}

void Game::moveAllTaskList(void) {
	if(finished_) {
		scene_list->clearTask();
		ship_list->clearTask();
		bomb_list->clearTask();
		submarine_list->clearTask();
		missile_list->clearTask();
		sea_list->clearTask();//~Submarine()がenableLine()を呼ぶのでSubmarineより後
		effect_list->clearTask();
		item_list->clearTask();
		finished_ = false;
		switch(gen_class_) {
			case NONE:
				break;
			case GAMEOVER:
				if(score_ranking_.back().second < getScore()) {
					new NameInputScene();
				} else {
					new GameoverScene();
				}
				break;
			case TITLE:
				new TitleScene();
				break;
			case GAME:
				new Sea();
				new Ship();
				new InformationScene();
				new PauseScene();
				break;
		};
		gen_class_ = NONE;
	}
	if(!paused_) {
		if(slow_) {
			++slow_count_;
		}
		if(!slow_ || 6 < slow_count_) {
			moveTaskList(sea_list);
			moveTaskList(ship_list);
			moveTaskList(bomb_list);
			moveTaskList(submarine_list);
			moveTaskList(missile_list);
			moveTaskList(effect_list);
			moveTaskList(item_list);
			slow_count_ = 0;
		}
	}
	moveTaskList(scene_list);
}

void Game::drawAllTaskList(void) {
	//描画順序はよく考えること(後に書いたほうで上書きされるため)
	drawTaskList(sea_list);
	drawTaskList(ship_list);
	drawTaskList(bomb_list);
	drawTaskList(submarine_list);
	drawTaskList(item_list);
	drawTaskList(missile_list);
	drawTaskList(effect_list);//エフェクトはスコア表示などよりは下のレイヤ
	drawTaskList(scene_list);//ポーズなどの関係上一番上
}

void Game::moveTaskList(TaskList *task_list) {
	for(TaskIterator it(task_list); it.hasNext(); ) {
		Task *task = static_cast<Task*>(it.next());
		if(!task->move()) it.remove();
	}
}

void Game::drawTaskList(TaskList *task_list) {
	for(TaskIterator it(task_list); it.hasNext(); ) {
		Task *task = it.next();
		task->draw();
	}
}

Sea* Game::getSea(void) {
	return sea_;
}

void Game::setSea(Sea *sea) {
	sea_ = sea;
}

Ship* Game::getShip(void) {
	return ship_;
}

void Game::setShip(Ship *ship) {
	ship_ = ship;
}

int Game::getScore(void) const {
	return score_;
}

void Game::addScore(int value) {
	score_ += value;
}

void Game::resetScore(void) {
	score_ = 0;
}

void Game::finishGame(void) {
	finished_ = true;
}

std::size_t Game::max(int num, ...) {
	va_list arg;
	std::size_t max_size = 0;
	va_start(arg, num);
	for(int i = 0; i < num; ++i) {
		std::size_t t;
		t = va_arg(arg, std::size_t);
		if(max_size < t) {
			max_size = t;
		}
	}
	va_end(arg);
	return max_size;
}

bool Game::getPaused(void) const {
	return paused_;
}

void Game::togglePause(void) {
	paused_ ^= true;
}

void Game::finishProgram(void) {
	finished_program_ = true;
}

void Game::setGenClass(Game::Classes c) {
	gen_class_ = c;
}

void Game::writeScoreRanking(void) {
	std::ofstream ofile("ranking", std::ios::out | std::ios::binary);
	if(!ofile.is_open()) {
		fprintf(stderr, "error: cannot open file 'ranking' to write\n");
		exit(1);
	}
	for(std::list<std::pair<std::string, unsigned long> >::iterator it = score_ranking_.begin(); it != score_ranking_.end(); ++it) {
		ofile.write(reinterpret_cast<const char*>(it->first.c_str()), sizeof(char) * MAX_NAME_LENGTH);
		ofile.write(reinterpret_cast<char*>(&(it->second)), sizeof(unsigned long));
	}
}

void Game::loadScoreRanking(void) {
	std::ifstream ifile("ranking", std::ios::in | std::ios::binary);
	//rankingがなかったらデフォルトのやつを作る
	if(!ifile.is_open()) {
		for(int i = 0; i < RANKING_SIZE; ++i) {
			score_ranking_.push_front(std::pair<std::string, unsigned long>("DEFAULT", 1000 * (i + 1) * (i + 1)));
		}
		writeScoreRanking();
		ifile.open("ranking", std::ios::in | std::ios::binary);
		if(!ifile.is_open()) {
			fprintf(stderr, "error: cannot open file 'ranking' to read\n");
			exit(1);
		}
	}
	score_ranking_.clear();
	for(int i = 0; i < RANKING_SIZE; ++i) {
		char name[MAX_NAME_LENGTH + 1];
		unsigned long score;
		ifile.read(name, sizeof(char) * MAX_NAME_LENGTH);
		name[MAX_NAME_LENGTH] = '\0';
		ifile.read(reinterpret_cast<char*>(&score), sizeof(unsigned long));
		score_ranking_.push_back(std::pair<std::string, unsigned long>(name, score));
	}
}

std::list<std::pair<std::string, unsigned long> > Game::getScoreRanking(void) {
	return score_ranking_;
}

void Game::insertScoreRanking(std::string name, unsigned long score) {
	for(std::list<std::pair<std::string, unsigned long> >::iterator it = score_ranking_.begin(); it != score_ranking_.end(); ++it) {
		if(it->second < score) {
			score_ranking_.insert(it, std::pair<std::string, unsigned long>(name, score));
			score_ranking_.pop_back();//一番下のものはランク外になる
			break;
		}
	}
}

void Game::toggleSlow(void) {
	slow_ ^= true;
}

