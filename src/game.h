#ifndef GAME_H
#define GAME_H

#include <list>
#include "draw.h"
#include "tasklist.h"
#include "input.h"
#include "sea.h"
#include "ship.h"
#include "bomb.h"
#include "submarine.h"
#include "missile.h"
#include "constants.h"

class Game {
public:
	enum Classes {NONE, GAMEOVER, TITLE, GAME};
	Draw draw;
	Input input;
	TaskList *scene_list;
	TaskList *sea_list;
	TaskList *ship_list;
	TaskList *bomb_list;
	TaskList *submarine_list;
	TaskList *missile_list;
	TaskList *effect_list;
	TaskList *item_list;
	static Game* gi(void);
	void run(void);
	Sea* getSea(void);//seaがない場合はNULLを返す
	void setSea(Sea *sea);
	Ship* getShip(void);//shipがない場合はNULLを返す
	void setShip(Ship *ship);
	int getScore(void) const;
	void addScore(int value);
	void resetScore(void);
	void finishGame(void);
	bool getPaused(void) const;
	void togglePause(void);
	void finishProgram(void);
	void setGenClass(Classes c);
	void writeScoreRanking(void);
	void loadScoreRanking(void);//スコアランキング読み出し、無ければデフォルトのを作る
	std::list<std::pair<std::string, unsigned long> > getScoreRanking(void);
	void insertScoreRanking(std::string name, unsigned long score);
	void toggleSlow(void);
private:
	Sea *sea_;
	Ship *ship_;
	std::list<std::pair<std::string, unsigned long> > score_ranking_;//スコアランキング、0番目が1位
	int score_;
	bool finished_;//今のフレームで1ゲーム終了するかのフラグ
	Game::Classes gen_class_;
	bool paused_;//ポーズ中かどうか
	bool finished_program_;//プログラムを終了するかどうか
	bool slow_;//スロー中かどうか
	int slow_count_;//スロー用のカウンタ
	//オブジェクトの生成を禁止する(シングルトン)
	Game(void) {};
	Game(const Game &game) {};
	Game& operator=(const Game &game) {};
	//すべてのタスクを動作させる
	void moveAllTaskList(void);
	void drawAllTaskList(void);
	//タスクリストを動作させる
	void moveTaskList(TaskList *task_list);
	void drawTaskList(TaskList *task_list);
	std::size_t max(int num, ...);
};

#endif //GAME_H

