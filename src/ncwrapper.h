#ifndef SCREEN_H_
#define SCREEN_H_

#include <cstdlib>
#include <errno.h>
#include <ncurses.h>

/* ncurses の初期化と終了時の定番処理 */
void app_init(void);
void app_exit(void);

/* 起動端末情報の取得 */
void get_screen_range(int &y, int &x);

#endif //SCREEN_H_

