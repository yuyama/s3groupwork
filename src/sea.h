#ifndef SEA_H
#define SEA_H

#include <vector>
#include "task.h"

class Sea : public Task {
public:
	Sea(void);
	virtual ~Sea(void);
	void* operator new(std::size_t t) throw();
	void operator delete(void *p);
	virtual bool move(void);
	void draw(void);
	void enableLine(int line);//指定した行(水面の1つ下から数える)を有効化する
protected:
	int getAvailableLine(void);//空いている行番号(水面の1つ下から数える)をランダムに返す
	void disableLine(int line);//指定した行(水面の1つ下から数える)を無効化する
private:
	std::vector<bool> free_levels_;//空行を保持(行は水面の1つ下から数える)
};

#endif //SEA_H

