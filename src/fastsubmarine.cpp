#include <cstdlib>
#include "fastsubmarine.h"
#include "game.h"
#include "straightmissile.h"

FastSubmarine::FastSubmarine(int y, int direction, int speed, int interval) : Submarine(y, direction, speed, interval) {
}

FastSubmarine::~FastSubmarine(void) {
}

bool FastSubmarine::move(void) {
	//移動する処理
	if(!Submarine::move()) return false;
	//ミサイル発射処理
	processMissile();
	return true;
}

void FastSubmarine::draw(void) {
	if (direction_ == RIGHT_AHEAD) {
		Game::gi()->draw.printString(x_, y_, "=>(^p^)");
	} else if (direction_ == LEFT_AHEAD) {
		Game::gi()->draw.printString(x_, y_, "(^q^)<=");
	}
}

void FastSubmarine::destroy(void) {
	Game::gi()->addScore(FAST_SCORE);  //スコア追加処理
}
