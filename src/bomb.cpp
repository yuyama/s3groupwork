#include "bomb.h"
#include "constants.h"
#include "taskiterator.h"
#include "submarine.h"
#include "game.h"
#include "crasheffect.h"

Bomb::Bomb(int x, int speed) : Task(Game::gi()->bomb_list),x_(x), speed_(speed) {
	y_=SCREEN_HEIGHT-WATER_LEVEL;
}

void* Bomb::operator new(std::size_t t) throw() {
	return operator_new(t, Game::gi()->bomb_list);
}

void Bomb::operator delete(void *p) {
	operator_delete(p, Game::gi()->bomb_list);
}

bool Bomb::move(void) {
	++counter_;
	if(speed_ <= counter_) {
		y_++;
		counter_ = 0;
	}
	if(y_>=SCREEN_HEIGHT){//bombが画面最下層に到達
		if(Game::gi()->getShip() != NULL) Game::gi()->getShip()->reloadStock();
		return false;
	}
	if(checkHitSubmarine()){
		if(Game::gi()->getShip() != NULL) Game::gi()->getShip()->reloadStock();
		return false;
	}

	return true;
}

void Bomb::draw(void){
	Game::gi()->draw.printString(x_, y_, "O");
}

bool Bomb::checkHitSubmarine(void){
	for(TaskIterator it(Game::gi()->submarine_list); it.hasNext(); ) {
		Submarine *submarine = dynamic_cast<Submarine*>(it.next());
		if(submarine->getX()<=x_&&x_<submarine->getX()+SUBMARINE_LENGTH&&y_==submarine->getY()){//潜水艦に命中
			new CrashEffect(submarine->getX() + 3, submarine->getY());
			submarine->destroy();
			it.remove();

			return true;
		}
	}
	return false;
}
