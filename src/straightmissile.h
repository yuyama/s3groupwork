#ifndef STRAIGHT_MISSILE_H
#define STRAIGHT_MISSILE_H

#include "missile.h"

class StraightMissile : public Missile {
public:
	StraightMissile(int x, int y);
	bool move(void);
	void draw(void);
};

#endif //STRAIGHT_MISSILE_H

