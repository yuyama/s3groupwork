#include <cstdio>
#include <cstdlib>
#include "submarine.h"
#include "game.h"
#include "constants.h"
#include "sea.h"
#include "stockitem.h"
#include "speeditem.h"
#include "straightmissile.h"

Submarine::Submarine(int y, int direction, int speed, int interval) : Task(Game::gi()->submarine_list), y_(y), counter_(0), interval_(interval) {
	direction_ = direction;
	speed_ = speed;
	speed_temp_ = 0;
	if (direction_ == RIGHT_AHEAD) {
		x_ = SUBMARINE_LENGTH * -1;
	} else if (direction_ == LEFT_AHEAD) {
		x_ = SCREEN_WIDTH;
	}
}

Submarine::~Submarine(void) {
	Game::gi()->getSea()->enableLine(y_ - (SCREEN_HEIGHT - WATER_LEVEL + 1));
}

bool Submarine::move(void) {
	speed_temp_++;

	if (speed_ == speed_temp_) {
		speed_temp_ = 0;

		if (direction_ == RIGHT_AHEAD) {
			x_++;
			if (x_ == SCREEN_WIDTH) {
				return false;
			}

		} else if (direction_ == LEFT_AHEAD) {
			x_--;
			if (x_ == SUBMARINE_LENGTH * -1) {
				return false;
			}
		}
	}
	return true;
}

void Submarine::processMissile(void) {
	++counter_;
	if(interval_ < counter_) {
		if(rand() % 10 == 0) {
			launchMissile();
			counter_ = 0;
		}
	}
}

void Submarine::launchMissile(void) {
	if (direction_ == RIGHT_AHEAD) {
		new StraightMissile(x_ + 1, y_);
	} else if (direction_ == LEFT_AHEAD) {
		new StraightMissile(x_ + 5, y_);
	}
}

int Submarine::getX(void) {
	return x_;
}

int Submarine::getY(void) {
	return y_;
}

int Submarine::getSpeed(void) {
	return speed_;
}

void* Submarine::operator new(std::size_t t) throw() {
	return operator_new(t, Game::gi()->submarine_list);
}

void Submarine::operator delete(void *p) {
	operator_delete(p, Game::gi()->submarine_list);
}

void Submarine::destroy(void) {
}

