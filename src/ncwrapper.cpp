#include "ncwrapper.h"
#include "constants.h"

void app_init(void) {
	if ( initscr() == NULL ) {
		perror("initscr");
		_Exit(EXIT_FAILURE);
	}
	cbreak();
	noecho();
	refresh();
	return;
}

void app_exit(void) {
	nodelay(stdscr,FALSE);
	echo();
	nocbreak();
	endwin();
	return;
}

void get_screen_range(int &y, int &x) {
	getmaxyx(stdscr,y,x);
	if ( x < SCREEN_WIDTH ) {
		app_exit();
		perror("small width in get_screen_range");
		_Exit(EXIT_FAILURE);
	}
	if ( y < SCREEN_HEIGHT ) {
		app_exit();
		perror("small height in get_screen_range");
		_Exit(EXIT_FAILURE);
	}
	return;
}

