#include "titlescene.h"
#include "game.h"
#include "constants.h"
#include "titlesea.h"
#include "creditscene.h"
#include "rankingscene.h"

TitleScene::TitleScene(void) : cursor_pos_(0) {
	new TitleSea();
}

TitleScene::TitleScene(int cursor_pos) : cursor_pos_(cursor_pos) {
	new TitleSea();
}

bool TitleScene::move(void) {
	//カーソル移動
	if(Game::gi()->input.getLeft()) --cursor_pos_;
	if(Game::gi()->input.getRight()) ++cursor_pos_;
	if(cursor_pos_ < 0) cursor_pos_ = 3;
	if(3 < cursor_pos_) cursor_pos_ = 0;
	//Quitを押したときの処理
	if(Game::gi()->input.getQuit()) {
		if(cursor_pos_ == 3) {//すでにQuitの位置にあったとき
			Game::gi()->finishProgram();
		} else {//他のメニューのとき
			cursor_pos_ = 3;
		}
	}
	if(Game::gi()->input.getBombLeft() || Game::gi()->input.getBombRight()) {
		Game::gi()->input.update();
		switch(cursor_pos_) {
			case 0://GameStart
				Game::gi()->input.update();
				Game::gi()->setGenClass(Game::GAME);
				Game::gi()->finishGame();
				Game::gi()->resetScore();
				break;
			case 1://Ranking
				new RankingScene();
				break;
			case 2://Credit
				new CreditScene();
				break;
			case 3://Quit
				Game::gi()->finishProgram();
			break;
		}
		return false;
	}
	return true;
}

void TitleScene::draw(void) {
  Game::gi()->draw.printString(SCREEN_WIDTH/2-TITLE_WIDTH/2,SCREEN_HEIGHT/2-TITLE_HEIGHT,
			       "Numazu                  ");
  Game::gi()->draw.printString(SCREEN_WIDTH/2-TITLE_WIDTH/2,SCREEN_HEIGHT/2-TITLE_HEIGHT+1,
			       "|  _ \\          _  ||   ");
  Game::gi()->draw.printString(SCREEN_WIDTH/2-TITLE_WIDTH/2,SCREEN_HEIGHT/2-TITLE_HEIGHT+2,
			       "| |  |___  ___ | _ ||__ ");
  Game::gi()->draw.printString(SCREEN_WIDTH/2-TITLE_WIDTH/2,SCREEN_HEIGHT/2-TITLE_HEIGHT+3,
			       "| |  | _ ||   \\| _|| _ \\");
  Game::gi()->draw.printString(SCREEN_WIDTH/2-TITLE_WIDTH/2,SCREEN_HEIGHT/2-TITLE_HEIGHT+4,
			       "| |_-| __|| | |||_ || ||");
  Game::gi()->draw.printString(SCREEN_WIDTH/2-TITLE_WIDTH/2,SCREEN_HEIGHT/2-TITLE_HEIGHT+5,
			       "|___-|___ | __/|__\\|| ||");
  Game::gi()->draw.printString(SCREEN_WIDTH/2-TITLE_WIDTH/2,SCREEN_HEIGHT/2-TITLE_HEIGHT+6,
			       "          ||            ");
  Game::gi()->draw.printString(SCREEN_WIDTH/2-TITLE_WIDTH/2,SCREEN_HEIGHT/2-TITLE_HEIGHT+7,
			       "          ||            ");
  const int h = SCREEN_HEIGHT / 2 + 5;
  Game::gi()->draw.printString(14 * 1, h - ((cursor_pos_ == 0) ? 1 : 0), "GAMESTART");
  Game::gi()->draw.printString(14 * 2, h - ((cursor_pos_ == 1) ? 1 : 0), "RANKING  ");
  Game::gi()->draw.printString(14 * 3, h - ((cursor_pos_ == 2) ? 1 : 0), "CREDIT   ");
  Game::gi()->draw.printString(14 * 4, h - ((cursor_pos_ == 3) ? 1 : 0), "QUIT     ");
  Game::gi()->draw.printString(14 * (cursor_pos_ + 1) - 1, h - 1, ">");
}

