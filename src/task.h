#ifndef TASK_H
#define TASK_H

#include <iostream>

class TaskList;
class TaskIterator;

class Task {
friend class TaskList;
friend class TaskIterator;
public:
	Task(TaskList *task_list);
	virtual ~Task(void);
	virtual bool move(void);
	virtual void draw(void);
protected:
	static void* operator_new(size_t t, TaskList *task_list);
	static void operator_delete(void *p, TaskList *task_list);
private:
	TaskList *task_list_;
	Task *prev_;
	Task *next_;
	void* operator new(std::size_t t) throw() {}
	void operator delete(void *p) {}
};

#endif //TASK_H

