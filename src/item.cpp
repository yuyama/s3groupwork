#include "item.h"
#include "game.h"

Item::Item(int x, int y) : Task(Game::gi()->item_list), x_(x), y_(y), count_(0) {
}

void* Item::operator new(std::size_t t) throw() {
	return operator_new(t, Game::gi()->item_list);
}

void Item::operator delete(void *p) {
	operator_delete(p, Game::gi()->item_list);
}

