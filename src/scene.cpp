#include "scene.h"
#include "game.h"

Scene::Scene(void) : Task(Game::gi()->scene_list) {
}

void* Scene::operator new(std::size_t t) throw() {
	return operator_new(t, Game::gi()->scene_list);
}

void Scene::operator delete(void *p) {
	operator_delete(p, Game::gi()->scene_list);
}

