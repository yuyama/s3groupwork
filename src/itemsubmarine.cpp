#include <cstdlib>
#include "itemsubmarine.h"
#include "game.h"
#include "stockitem.h"
#include "speeditem.h"

ItemSubmarine::ItemSubmarine(int y, int direction, int speed) : Submarine(y, direction, speed, 0) {
}

ItemSubmarine::~ItemSubmarine(void) {
}

bool ItemSubmarine::move(void) {
	//移動する処理
	if(!Submarine::move()) return false;
	return true;
}

void ItemSubmarine::draw(void) {
	if (direction_ == RIGHT_AHEAD) {
		Game::gi()->draw.printString(x_, y_, "=( #O_)");
	} else if (direction_ == LEFT_AHEAD) {
		Game::gi()->draw.printString(x_, y_, "(_O# )=");
	}
}

void ItemSubmarine::destroy(void) {
	Game::gi()->addScore(ITEM_SUBMARINE_SCORE);  //スコア追加処理
	//アイテム生成処理
	if(rand() % 2 == 0) {
		new StockItem(x_ + 3, y_);
	} else {
		new SpeedItem(x_ + 3, y_);
	}
}

