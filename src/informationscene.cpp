#include "informationscene.h"
#include "game.h"
#include "constants.h"
#include "sea.h"

bool InformationScene::move(void) {
	return true;
}

void InformationScene::draw(void) {
	int x;
	if(Game::gi()->getShip() == NULL) return;
	x = SCREEN_WIDTH/2-Game::gi()->getShip()->getMaxStock()/2;
	for(int i = 0; i<Game::gi()->getShip()->getStock(); i++){
		Game::gi()->draw.printString(x + i, 1, "O");
	}
	x = SCREEN_WIDTH-15;
	Game::gi()->draw.printString(x, 0, "SCORE:%8d", Game::gi()->getScore());
}

