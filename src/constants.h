#ifndef CONSTANTS_H
#define CONSTANTS_H

//画面に描画されるサイズ
const int SCREEN_WIDTH = 80;
const int SCREEN_HEIGHT = 24;

//タイトルのロゴの幅
const int TITLE_WIDTH = 24;
const int TITLE_HEIGHT = 6;

//フレームごとのウェイトの長さ(ミリ秒)
const int SLEEP_TIME = 50;

//タスクリストのサイズ
const int MAX_NUM_SCENE = 3;
const int MAX_NUM_SEA = 1;
const int MAX_NUM_SHIP = 1;
const int MAX_NUM_BOMB = 32;
const int MAX_NUM_SUBMARINE = 32;
const int MAX_NUM_MISSILE = 64;
const int MAX_NUM_EFFECT = 16;
const int MAX_NUM_ITEM = 16;

//水位
const int WATER_LEVEL = 20;

//Shipの長さ
const int SHIP_LENGTH = 5;

//潜水艦・ゲームバランス
const int RIGHT_AHEAD = 1;
const int LEFT_AHEAD = 0;
const int SUBMARINE_LENGTH = 7;
const int BASIC_SPEED = 2;
const int MAX_SPEED = 8;

const int SAFETY_SEA_LEVEL = 3;

const int DEFAULT_MAX_STOCK = 4;
const int TRULY_MAX_STOCK = 16;
const int DEFAULT_BOMB_SPEED = 8;

const int THREE_WAY_TURN_UP = 100000;
const int FAST_TURN_UP = 20000;

//スコア
const int NORMAL_SCORE = 1000;
const int ITEM_SUBMARINE_SCORE = 2000;
const int FAST_SCORE = 3000;
const int THREE_WAY_SCORE = 10000;

//名前の最大長
const int MAX_NAME_LENGTH = 8;

//ランキングの登録数
const int RANKING_SIZE = 10;


#endif //CONSTANTS_H

