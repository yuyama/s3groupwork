#include "taskiterator.h"
#include "tasklist.h"
#include "task.h"

TaskIterator::TaskIterator(TaskList *task_list) : task_list_(task_list), task_(task_list->active_task_) {
}

bool TaskIterator::hasNext(void) const {
	return task_->next_ != task_list_->active_task_;
}

Task* TaskIterator::next(void) {
	return task_ = task_->next_;
}

void TaskIterator::remove(void) {
	task_ = task_->prev_;
	delete task_->next_;
}

