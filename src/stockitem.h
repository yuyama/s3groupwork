#ifndef STOCK_ITEM_H
#define STOCK_ITEM_H

#include "item.h"

class StockItem : public Item {
public:
	StockItem(int x, int y);
	bool move(void);
	void draw(void);
};

#endif //STOCK_ITEM_H

