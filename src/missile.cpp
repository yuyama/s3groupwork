#include "missile.h"
#include "game.h"
#include "constants.h"

Missile::Missile(int x, int y):Task(Game::gi()->missile_list),x_(x), y_(y){

}

void* Missile::operator new(std::size_t t) throw() {
	return operator_new(t, Game::gi()->missile_list);
}

void Missile::operator delete(void *p){
	operator_delete(p, Game::gi()->missile_list);
}

bool Missile::move(void){
	//当たり判定と消滅処理
	if(Game::gi()->getShip() != NULL) {
		if(y_ == SCREEN_HEIGHT - WATER_LEVEL && Game::gi()->getShip()->getX()<=x_ && Game::gi()->getShip()->getX()+SHIP_LENGTH>x_){
			Game::gi()->ship_list->clearTask();
			return false;
		}
	}
	if(y_ < SCREEN_HEIGHT - WATER_LEVEL){//Y座標が水面を越えたとき
		return false;
	}
	return true;
}

