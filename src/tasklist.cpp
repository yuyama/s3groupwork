#include "tasklist.h"
#include "taskiterator.h"
#include "task.h"

TaskList::TaskList(int max_task_size, int max_num_task) : max_task_size_(max_task_size), max_num_task_(max_num_task), num_free_task_(max_num_task) {
	buf_ = new char[max_task_size_ * (max_num_task_ + 2)];
	active_task_ = reinterpret_cast<Task*>(buf_ + max_task_size_ * 0);
	active_task_->prev_ = active_task_;
	active_task_->next_ = active_task_;
	free_task_ = reinterpret_cast<Task*>(buf_ + max_task_size_ * 1);
	for(int i = 1; i < max_num_task_ + 1; ++i) {
		reinterpret_cast<Task*>(buf_ + max_task_size_ * i)->next_ = reinterpret_cast<Task*>(buf_ + max_task_size_ * (i + 1));
	}
	reinterpret_cast<Task*>(buf_ + max_task_size_ * (max_num_task_ + 1))->next_ = free_task_;
}

TaskList::~TaskList(void) {
	delete buf_;
}

int TaskList::getNumFreeTask(void) const {
	return num_free_task_;
}

int TaskList::getNumActiveTask(void) const {
	return max_num_task_ - num_free_task_;
}

void TaskList::clearTask(void) {
	for(TaskIterator i(this); i.hasNext(); i.next(), i.remove());
}

