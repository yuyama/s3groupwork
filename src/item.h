#ifndef ITEM_H
#define ITEM_H

#include "task.h"

class Item : public Task {
public:
	Item(int x, int y);
	void* operator new(std::size_t t) throw();
	void operator delete(void *p);
protected:
	int x_;
	int y_;
	int count_;
};

#endif //ITEM_H

