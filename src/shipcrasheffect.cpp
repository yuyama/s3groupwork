#include "shipcrasheffect.h"
#include "game.h"

ShipCrashEffect::ShipCrashEffect(int x, int y) : CrashEffect(x, y) {
	Game::gi()->toggleSlow();
}

ShipCrashEffect::~ShipCrashEffect(void) {
	Game::gi()->toggleSlow();
	Game::gi()->scene_list->clearTask();
	Game::gi()->setGenClass(Game::GAMEOVER);
	Game::gi()->finishGame();
}

