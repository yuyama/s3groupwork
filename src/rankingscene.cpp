#include "rankingscene.h"
#include "game.h"
#include "titlescene.h"

bool RankingScene::move(void) {
	//何か押されたらタイトルに戻る
	if(Game::gi()->input.getLeft() ||
			Game::gi()->input.getRight() ||
			Game::gi()->input.getBombLeft() ||
			Game::gi()->input.getBombRight() ||
			Game::gi()->input.getPause() ||
			Game::gi()->input.getQuit()) {
		Game::gi()->input.update();
		new TitleScene(1);
		return false;
	}
	return true;
}

void RankingScene::draw(void) {
	std::list<std::pair<std::string, unsigned long> > ranking = Game::gi()->getScoreRanking();
	{//forで2つの変数を宣言できないので
		int i = 0;
		for(std::list<std::pair<std::string, unsigned long> >::iterator it = ranking.begin(); it != ranking.end(); ++it, ++i) {
			Game::gi()->draw.printString((SCREEN_WIDTH - 20) / 2, (SCREEN_HEIGHT - RANKING_SIZE) / 2 + i, "%2d %8s %08d", i + 1, it->first.c_str(), it->second);
		}
	}
}

