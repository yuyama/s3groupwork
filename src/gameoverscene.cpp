#include "gameoverscene.h"
#include "titlescene.h"
#include "game.h"
#include "constants.h"
#include "sea.h"
#include "informationscene.h"

GameoverScene::GameoverScene(void) {
}

bool GameoverScene::move(void) {
	if(Game::gi()->input.getQuit()) {
		Game::gi()->input.update();
		Game::gi()->setGenClass(Game::TITLE);
		Game::gi()->finishGame();
		return false;
	}
	return true;
}

void GameoverScene::draw(void) {
	std::string gameover_text = "[ GAME OVER ]";
	Game::gi()->draw.printString((SCREEN_WIDTH - gameover_text.length()) / 2, SCREEN_HEIGHT / 2, gameover_text.c_str());
	Game::gi()->draw.printString(SCREEN_WIDTH / 2 - 7, SCREEN_HEIGHT / 2 + 2, "SCORE:%8d", Game::gi()->getScore());
	gameover_text = "PUSH Q KEY TO QUIT";
	Game::gi()->draw.printString((SCREEN_WIDTH - gameover_text.length()) / 2, SCREEN_HEIGHT / 2 + 4, gameover_text.c_str());
}

