#ifndef CRASH_EFFECT_H
#define CRASH_EFFECT_H

#include "effect.h"

class CrashEffect : public Effect {
public:
	CrashEffect(int x, int y);
	bool move(void);
	void draw(void);
private:
	int x_;
	int y_;
	int counter_;
};

#endif //CRASH_EFFECT_H

