#ifndef SHIP_H
#define SHIP_H

#include <stddef.h>
#include "task.h"

class Ship : public Task {
public:
	Ship(void);
	~Ship(void);
	void* operator new(std::size_t t) throw();
	void operator delete(void *p);
	bool move(void);
	void draw(void);
	int getX(void) const;
	int getStock(void) const;
	int getMaxStock(void) const;
	void reloadStock(void);
	void incrementMaxStock(void);
	void increaseSpeed(void);
private:
	int x_;
	int stock_;
	int max_stock_;
	int bomb_speed_;
};

#endif //SHIP_H

