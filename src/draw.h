#ifndef DRAW_H
#define DRAW_H

#include <string>

class Draw {
public:
	Draw(void);
	void printString(int x, int y, const char *fmt, ...);//文字列描画
	void clearScreen(void);//画面消去
private:
	//画面サイズ
	int wx_;
	int wy_;
	//描画基準(左上)(reference)
	int rx_;
	int ry_;
	//1回でもclearScreenが呼ばれたか
	bool has_cleard;
};

#endif //DRAW_H

